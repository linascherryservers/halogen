FROM ubuntu:16.04
#FROM python:2
# darom is Ubuntu, o ne python image, nes taip gaunasi ant >200MB mazesnis :)
# MAINTAINER linas.lesauskas@cherryservers.com
RUN apt update && apt install -y python python-pip python-dev \
    && pip install --upgrade pip \
    && pip install docker \
    && pip install requests \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt
