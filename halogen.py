"""HALoGen - HAL'u generatorius. Palaukia 5sek, pasiima subrendusia uzduoti is dbtaip,
lygiagreciai paleidzia hal dockerius komutatoriu konfiguracijos keitimui
"""
import json
import multiprocessing
import os
import time

import docker
import requests
import urllib3

from halogenconf import *

## from halogenconf:
## BASE_URL 
## DBTAIPHOST
## LOGININFO 


MAX_WORKERS = 16
TIME_TO_WAIT = 15  ## kiek sek. turi laukti halogenas, atsarga, kad nekrenta naujos uzduotys
TLS_CONFIG = docker.tls.TLSConfig(
    client_cert=('/opt/dbtaip.crt', '/opt/dbtaip.key'),
    verify=False
    )
CLIENT = docker.DockerClient(base_url=BASE_URL, tls=TLS_CONFIG)
VOLUME = {'/tftp/': {'bind': '/tftp', 'mode': 'rw'}}
CONTEXT_KEYS = {"vlan":"-v", "interface":"-i", "acl":"-i", "global":"-z"}
HAL_IMAGE = 'hal:20180211'


def mp_worker(swobj):
    """vientinis workeris, gauna task'u iteratoriu
    """
    containers = []

    for task in swobj["prm"]:
        ctres = {}
        session_id = os.urandom(4).encode('hex')
        try:
            """ tam atvejui, jei dbtaip pradeda naudoti naujus kontekstus,
            o halogenas dar apie tai nezino. kad neluztu kitos uzduotys"""
            context_key = CONTEXT_KEYS[task["context"]]
        except Exception:
            context_key = '-evil'

        docker_command = 'python dell_manage.py %s -s %s -m %s %s %s %s  ' % \
            (task["context"], swobj["swip"], swobj["model"], \
            context_key, task["mainvar"], task["morevars"])
	         
        print session_id, docker_command
        ctres["id"] = task["id"]
        ctres["swip"] = swobj["swip"]
        ctres["iter"] = task["iter"] + 1

        try:
            container = CLIENT.containers.run(HAL_IMAGE, docker_command, \
                remove=True, volumes=VOLUME, stderr=True)
            ctres["status"] = "done"
            ctres["output"] = container
        except Exception as erroras:
            print erroras
            ctres["status"] = "failed"
            ctres["output"] = str(erroras)
        finally:
            print "%s container finaly: %s - %s" % (session_id, ctres["status"], ctres["output"])
            
        containers.append(ctres)
    return containers

def mp_handler(dataa):

    """ workeriu handleris, sustato uzduociu eiles workeriu poolui """
    pool = multiprocessing.Pool(MAX_WORKERS)
    results = pool.map(mp_worker, dataa)
    flatresult = []
    for workers in results:
        for tasks in workers:
            flatresult.append(tasks)
    print json.dumps(flatresult, indent=4)

    return flatresult

def post_results(token, res):
    """sudeda tasku rezultatus atgal i dbtaip
    """
    data = {"response":res}
    response = requests.post(DBTAIPHOST+'/rpc/halogen_result', json=data,\
        headers={'Authorization':'Bearer '+token})
    print response.text

def login():
    """pasisveikinimas su dbtaip. JWT tokenas bus naudojamas 2 kart -
    task'u gavimui ir rezultato padejimui
    """
    urllib3.disable_warnings()  # sitas reikalingas, kol ner CA sertifikato.

    jwtr = requests.post(DBTAIPHOST+'/rpc/login', data=LOGININFO)
    token = jwtr.json()[0]['token']
    return token

def get_tasks(token):
    """gaunam multilevel array. sugrupuota pagal sw ip adresa.
    """
    data = {"_params":""}
    response = requests.post(DBTAIPHOST+'/rpc/halogen_tasklist', json=data,\
        headers={'Authorization':'Bearer '+token})

    mybigdata = response.json()

    return mybigdata

if __name__ == '__main__':
    time.sleep(TIME_TO_WAIT)
    # po paleidimo pamiegam, ir tik tada prasom uzduoties.
    # jei per ta laika dar papildomai ikrito nauju,
    # backends nieko neduos - visos uzduotys atiteks sekanciam halogenui
    __token__ = login()
    __tasks__ = get_tasks(__token__)
    
    if __tasks__:
        __results__ = mp_handler(__tasks__)
        post_results(__token__, __results__)
