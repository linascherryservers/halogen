# halogen

python_dell_expect (HAL) generatorius.

single shot docker containeris, kuris susirenka iš dbtaip komutatorių pergeneravimo užduotis, lygiagrečiai startuoja keliolika "hal" konteinerių darbui ir jų gautus rezultatus sukelia į dbtaip.

Prieš paleidžiant į produkciją iš git, reikia:

1. sudėti teisingus login halogenconf.py
2. sudėti galiojančius sertifikatus kreipimuisį į Docker hostą hal'ų kvietimui
